﻿using OpenMessengerNET.network.core;
using OpenMessengerNET.utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OpenMessengerNET
{
  static class Program
  {
    private static List<Task> listOfTasks = new List<System.Threading.Tasks.Task>();
    public static tcpserver Notification;
    public static tcpserver Switchboard;

    public static async Task AsyncAddTask(Task t)
    {
      await Task.Run(() => listOfTasks.Add(t));
    }

    public static void AddTask(Task t)
    {
      listOfTasks.Add(t);
    }

    async static Task Main(string[] args)
    {
      await AsyncSupport.SetForegroundColour(ConsoleColor.White);

      await ConfigFileHandler.GetConfiguration();

      Notification = new tcpserver(1863);
      Switchboard = new tcpserver(1864, true);

      //TODO: start up all tcp servers here //
      listOfTasks.Add(Notification.StartServer());
      listOfTasks.Add(Switchboard.StartServer());

      await AsyncSupport.WriteLine("Server started!");

      await Task.WhenAll(listOfTasks);
    }
  }
}
