﻿using OpenMessengerNET.database;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace OpenMessengerNET
{
  /// <summary>
  /// TODO: Implement configuration file parsing.
  /// </summary>
  public static class ConfigFileHandler
  {
    public static string LocalFileDatabasePath = Environment.CurrentDirectory + "/data/";
    public static string SwitchboardIP = "127.0.0.1:1864";

    public static async Task<IDatabaseType> GetDatabase()
    {
      return await Task.Run(() =>
      {
        // We implement using the harddrive as a database first.
        // Really need to implement the config file handler.
        return new LocalFileDB();
      });
    }

    public static async Task GetConfiguration()
    {
      // Not implemented yet just use a simple file
      await Task.Run(() =>
      {
        if (!File.Exists(Environment.CurrentDirectory + "/switchboardip.txt"))
        {
          // all of this is temp code to allow lan testing.
          File.Create(Environment.CurrentDirectory + "/switchboardip.txt");
          File.WriteAllText(Environment.CurrentDirectory + "/switchboardip.txt", SwitchboardIP);
        }

        SwitchboardIP = File.ReadAllText(Environment.CurrentDirectory + "/switchboardip.txt");
      });
    }

  }
}
