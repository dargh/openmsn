﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OpenMessengerNET.utilities
{
  public static class Hash
  {
    /// <summary>
    /// Creates a MD5 hash from a string.
    /// https://msdn.microsoft.com/en-us/library/system.security.cryptography.md5%28v=vs.110%29.aspx
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static async Task<string> CreateMD5(string input)
    {
      return await Task.Run(() =>
      {
        // Use input string to calculate MD5 hash
        using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
        {
          byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
          byte[] hashBytes = md5.ComputeHash(inputBytes);

          // Convert the byte array to hexadecimal string
          StringBuilder sb = new StringBuilder();
          for (int i = 0; i < hashBytes.Length; i++)
          {
            sb.Append(hashBytes[i].ToString("X2"));
          }

          // Ensure this is lowercase for challenge validating.
          return sb.ToString().ToLower();
        }
      });
    }
  }
}
