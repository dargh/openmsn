﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMessengerNET.utilities
{
  public static class ChallengeMaker
  {
    public static Random rnd = new Random();

    /// <summary>
    /// This challenge generator is intended for 
    /// Notification and Dispatch.
    /// </summary>
    /// <returns></returns>
    public static async Task<string> MakeNormalChallenge()
    {
      return await Task.Run(() =>
      {
        // This is really dumb. But it looks close to the real Microsoft generated challenges.
        return $"{rnd.Next(10000, Int32.MaxValue)}.{rnd.Next(10000, Int32.MaxValue)}.{rnd.Next(10000, Int32.MaxValue)}";
      });
    }

    public static async Task<string> CreateSessionKey()
    {
      return await Task.Run(() =>
      {
          const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.";
          return new string(Enumerable.Repeat(chars, rnd.Next(25,35))
            .Select(s => s[rnd.Next(s.Length)]).ToArray());
      });
    }
  }
}
