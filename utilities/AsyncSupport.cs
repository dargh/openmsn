﻿using System;
using System.Threading.Tasks;


/*
Sadly most .net features are all sync so we must make them into Async tasks.
This below is a class that takes the place of common functions.
*/

namespace OpenMessengerNET.utilities
{
  /// <summary>
  /// Ports Sync functions to Async
  /// </summary>
  public static class AsyncSupport
  {
    /// <summary>
    /// Set's text output colour.
    /// </summary>
    /// <param name="col"></param>
    public static async Task SetForegroundColour(ConsoleColor col)
    {
      await Task.Run(() => Console.ForegroundColor = col);
    }

    /// <summary>
    /// Outputs text to console display.
    /// </summary>
    /// <param name="text"></param>
    public static async Task WriteLine(string text)
    {
      await Task.Run(() => Console.WriteLine(text));
    }
  }

  public static class Utilty
  {
    public enum SyntaxIssue
    {
      Expected,
      TooManyInputs,
      MissingInputs
    }

    public static SyntaxIssue HasRequiredInputs(string[] split, int requires)
    {
      if (split.Length == requires)
        return SyntaxIssue.Expected;
      else if (split.Length < requires)
        return SyntaxIssue.MissingInputs;
      else if (split.Length > requires)
        return SyntaxIssue.TooManyInputs;
      else
        return SyntaxIssue.MissingInputs; // Yes the compiler loves this line. Too bad!
    }
  }
}
