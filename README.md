## Introduction

This is a MSN Messenger server that is early in development.
It's also my first time working with async so expect it to be bad.

| Protocol | Supported | Tested |
| -------- | -------- | -------- |
| MSNP1   | ✅   |   Yes   |
| MSNP2   | ✅    | Yes   |
| MSNP3   | ❌   | No   |
| MSNP4   | ❌   |  No   |
| MSNP5   | ❌   |  No   |

**MSNP5 support is planned as MSNP1 - 4 are very simliar and will not take long to implement**

# Release Builds

Not yet but check the pipeline.

# Other MSN Server Projects

This is a list of other MSN server emulators that I think deserve some light.

- [Escargot](https://gitlab.com/escargot-chat)