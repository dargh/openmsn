﻿using OpenMessengerNET.database;
using OpenMessengerNET.utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace OpenMessengerNET.network.core
{
  public class Key
  {
    public Key(string handle = null, string k = null)
    {
      user = handle;
      key = k;
    }
    public string user = null;
    public string key = null;
  }
  public class Session
  {
    public string SessionID = null;
    public string Starter = null;
    public DateTime LastMsgTime = DateTime.Now;
    public List<string> Users = new List<string>();
    public List<Key> Keys = new List<Key>();

    public async Task<Key> NewKey(string handle)
    {
      return await Task.Run(async () =>
      {
        Key temp = new Key(handle, await ChallengeMaker.CreateSessionKey());
        Keys.Add(temp);
        return temp;
      });
    }

    public async Task<Key> GetKey(string handle)
    {
      return await Task.Run(() =>
      {
        for (int i = 0; i < Keys.Count; ++i)
        {
          if (Keys[i].user == handle)
            return Keys[i];
          else
            continue;
        }
        return null;
      });
    }
  }

  public class tcpserver
  {
    public List<UserWrapper> Users = new List<UserWrapper>();
    public TcpListener tcp;
    public bool IsSwitchboard = false;
    public bool ShouldShutdown = false;

    // Switchboard
    public List<Key> LoginKeys;
    public List<Session> Sessions;

    public IDatabaseType DB = null;

    public tcpserver(int port = 1806, bool sb = false)
    {
      IsSwitchboard = sb;
      if (sb)
      {
        Sessions = new List<Session>();
        LoginKeys = new List<Key>();
      }
#pragma warning disable CS0618
      tcp = new TcpListener(port); // Type or member is obsolete
#pragma warning restore CS0618

      tcp.Server.NoDelay = true;
      tcp.Server.DontFragment = true;
      tcp.Server.Blocking = true;
    }

    public async Task<Key> CreateLoginKey(string handle)
    {
      return await Task.Run(async () =>
      {
        Key NewKey = new Key();
        NewKey.user = handle;
        NewKey.key = await ChallengeMaker.CreateSessionKey();
        LoginKeys.Add(NewKey);
        return NewKey;
      }); 
    }

    public async Task<string> CreateSession(string Starter)
    {
      return await Task.Run(async () =>
      {
        Key MyKey = new Key();
        MyKey.user = Starter;
        MyKey.key = await ChallengeMaker.CreateSessionKey();
        Session Session = new Session();
        Session.SessionID = (Sessions.Count + 1).ToString();
        Session.Starter = Starter;
        Session.Users.Add(Starter);
        Session.Keys.Add(MyKey);
        Sessions.Add(Session);
        return Session.SessionID;
      });
    }

    public async Task<Session> GetSessionByID(string sessionid)
    {
      return await Task.Run(() =>
      {
        for (int i = 0; i < Sessions.Count; ++i)
        {
          if (Sessions[i].SessionID == sessionid)
            return Sessions[i];
          else
            continue;
        }
        return null;
      });
    }

    public async Task<int> GetOnlineUserIndex(string handle)
    {
      return await Task.Run(() =>
      {
        for (int i = 0; i < Users.Count; ++i)
        {
          UserWrapper user = Users[i];

          if (user.info != null)
          {
            if (user.info.handle == handle)
              return i;
            else
              continue;
          }
        }
        return -1;
      });
    }

    public async Task DealWithClients()
    {
      await Task.Run(async () =>
      {
        while (!ShouldShutdown)
        {
          TcpClient client = await tcp.AcceptTcpClientAsync();
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
          ClientThink(client.Client);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }
      });
    }

    public async Task PingClient(UserWrapper user)
    {
        await Task.Run(async () =>
        {
          while (await user.IsConnected())
          {
            // 35 second ping to keep the connection open.
            if (IsSwitchboard)
              await Task.Delay((60000*5)); // 5 minutes for switchboard.
            else
              await Task.Delay(35000); // 35 seconds for notification.

            if (!user.ShouldProcessPackets)
            {
              // No need to ping as this usually means the connection is active.
              user.PassedPingCheck = true;
              continue;
            }

            if (user.PassedPingCheck)
            {
              user.PassedPingCheck = false;
              await user.Send("PNG");
            }
            else
            {
              // Kick clients that do not respond back to the ping.
              await Task.Run(() =>
              {
                try
                {
                  user.MySocket.Disconnect(false);
                }
                catch (Exception)
                {
                // Client disconnected before we could boot them.
              }
              });
              break;
            }
          }
        });
    }

    public async Task PacketCheckClients(UserWrapper user)
    {
      while (await user.IsConnected())
      {
        await Task.Delay(5000); // (120000) = 2 minutes
        await Task.Run(async () =>
        {
          if (user.BadPackets >= 5)
          {
            try
            {
              await AsyncSupport.WriteLine($"FORCED DISCONNECT: {user.BadPackets}/5 bad packets allowed.");
              user.MySocket.Disconnect(false);
            }
            catch (Exception)
            {
              // Client disconnected before we could boot them.
              return;
            }
          }
          else
          {
            user.BadPackets = 0;
          }
        });
      }
    }

    public virtual async Task ClientThink(object sock)
    {
      await Task.Run(async () =>
      {
        // This first part is okay if blocking. It's the setup phase.
        UserWrapper user = new UserWrapper((Socket)sock);
        if (IsSwitchboard)
          user.Protocol.IsSwitchboard(true);
        else
          user.Protocol.IsSwitchboard(false);
        Users.Add(user);

        // Ping clients
        PingClient(user);
        PacketCheckClients(user);

        string data = "";

        while (await user.IsConnected())
        {
          // Blame the switchboard for using the same protocol almost. //
          if (!user.ShouldProcessPackets)
          {
            // Give slow connections plenty of time. //
            await Task.Delay(1000);
            continue;
          }

          try
          {
            data = await Task.Run(async () => 
            {
              try
              {
                return await user.reader.ReadLineAsync();
              }
              catch (IOException)
              {
                return null;
              }
            });
           
            if (data == null || data == "")
              continue;

            await AsyncSupport.WriteLine($"C: {data}");

            // Don't await. Allows us to handle multiple packets at once.
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            await user.Protocol.HandleData(data);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
          }
          catch (IOException)
          {
            break;
          }
          catch (Exception ex)
          {
            // Always disconnect when an error occurs to avoid damage to database //
            await AsyncSupport.SetForegroundColour(ConsoleColor.Red);
            await AsyncSupport.WriteLine(ex.Message);
            await AsyncSupport.SetForegroundColour(ConsoleColor.White);
            break;

          }

          if (await user.IsConnected() == false)
            break;
        }
        
        await AsyncSupport.WriteLine("Client disconnected.");

        if (user.IsLoggedIn && !IsSwitchboard)
        {
          await DB.SaveUser(user.info);
          await DB.SaveLists(user.UserLists, user.info);
        }
        else if (user.IsLoggedIn && IsSwitchboard)
        {
          // Alert people
          if (user.MySession != null)
          {
            user.MySession.Users.Remove(user.info.handle);
            foreach (string contact in user.MySession.Users)
            {
              int check = await Program.Switchboard.GetOnlineUserIndex(contact);

              if (check != -1)
              {
                await Program.Switchboard.Users[check].Send($"BYE {user.info.handle}");
              }
            }
            if (user.MySession.Users.Count == 0)
              Program.Switchboard.Sessions.Remove(user.MySession);
            else if (user.MySession.Users.Count == 1)
            {
              // Silently kill this session.
              int us = await Program.Switchboard.GetOnlineUserIndex(user.MySession.Users[0]);

              if (us == -1)
              {
                user.MySession.Users.Clear();
                Program.Switchboard.Sessions.Remove(user.MySession);
              }
              else
              {
                await Program.Switchboard.Users[us].Send($"BYE {Program.Switchboard.Users[us].info.handle}");
                try
                {
                  Program.Switchboard.Users[us].MySocket.Disconnect(false);
                }
                catch (Exception ex) { }
                user.MySession.Users.Clear();
                Program.Switchboard.Sessions.Remove(user.MySession);
              }
            }
            user.MySession = null;
          }
        }

        await Task.Run(() =>
        {
          Users.Remove(user);

          try
          {
            user.reader.Dispose();
          }
          catch { }

          try
          {
          user.writer.Dispose();
          }
          catch { }
          
          try
          {
          user.Client.Dispose();
          }
          catch { }
          // Garbage Collector //
          GC.Collect();
          GC.WaitForPendingFinalizers();
        });
        await AsyncSupport.WriteLine("Clean up finished on previous client.");
      });
    }

    public async Task StartServer()
    {
      await Task.Run(async () =>
      {
        await Task.Run(() => tcp.Start());

        // We should properly setup databse stuff here
        // But since I just want to implement the protocols first we use folders and files.
        DB = await ConfigFileHandler.GetDatabase();

        DB.CreateTestAccounts();

        if (!IsSwitchboard)
          await AsyncSupport.WriteLine($"Notification server started, 127.0.0.1:1863");
        else
          await AsyncSupport.WriteLine($"Switchboard server started, 127.0.0.1:1864");
        await DealWithClients();
      });
    }
  }
}
