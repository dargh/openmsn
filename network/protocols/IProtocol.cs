﻿using OpenMessengerNET.network.protocols.payloads;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OpenMessengerNET.network.protocols
{
  public interface IProtocol
  {
    Task ProtocolNumber();
    Task ProtocolString();
    Task HandleData(string data);

    Task IsSwitchboard(bool sb);

    // These are to support forwards and backwards clients.
    Task SendNameChange(string from, string to, string name);
    Task SendStatusChange(string from, string to, string status);
    Task LogonStatusChange(string from, string to, string status);
    Task LogoffStatusChange(string from, string to, string status);
    Task SendListChange(string from, string to, string list, bool AddToList);
    Task SendRingTo(string from, string to, string sessionid, string switchboardip, string key);

    // Switchboard
    Task SendMsgPayload(string from, string to, PayloadMSG msg);
    Task SendUserJustJoined(string from, string to, bool AlreadyExistingUser,string transaction, int max = -1, int current = -1);
  }
}
