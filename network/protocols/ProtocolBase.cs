﻿using OpenMessengerNET.database;
using OpenMessengerNET.database.wrappers;
using OpenMessengerNET.network.core;
using OpenMessengerNET.network.protocols.payloads;
using OpenMessengerNET.utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OpenMessengerNET.network.protocols
{
  public class ProtocolBase : IProtocol
  {
    public string[] split;
    public UserWrapper user;
    public IDatabaseType DB;
    public List<Key> AuthKeys;

    bool Switchboard = false;

    public ProtocolBase(UserWrapper me, bool IsSB = false)
    {
      Switchboard = IsSB;
      user = me;
      DB = Program.Notification.DB;
    }

    public async Task HandleData(string data)
    {
      await Task.Run(async () => {
        split = data.Split(' ');

        if (!Switchboard)
        {
          switch (split[0])
          {
            case "VER":
              await DoVER(split, data);
              break;
            case "INF":
              await DoINF(split, data);
              break;
            case "USR":
              await DoUSR(split, data);
              break;
            case "SYN":
              await DoSYN(split, data);
              break;
            case "CVR":
              await DoCVR(split, data);
              break;
            case "CHG":
              await DoCHG(split, data);
              break;
            case "ADD":
              await DoADD(split, data);
              break;
            case "REM":
              await DoREM(split, data);
              break;
            case "SND":
            case "FND":
              await DoFND(split, data);
              break;
            case "REA":
              await DoREA(split, data);
              break;
            case "OUT":
              await DoOUT(split, data);
              break;
            case "QNG":
              await DoQNG(split, data);
              break;
            case "XFR":
              await DoXFR(split, data);
              break;
            default:
              await UnknownPacket(split, data);
              break;
          }
        }
        else
        {
          // Switchboard here.
          switch (split[0])
          {
            case "ANS":
              //user.ShouldProcessPackets = false;
              await DoANS(split, data);
              break;
            case "QNG":
              await DoQNG(split, data);
              break;
            case "MSG":
              //await Task.Run(() => user.ShouldProcessPackets = false);
              await DoMSG(split, data);
              break;
            case "USR":
              await DoUSR(split, data);
              break;
            case "CAL":
              await DoCAL(split, data);
              break;
            case "OUT":
              await DoOUT(split, data);
              break;
            default:
              await UnknownPacket(split, data);
              break;
          }
        }

      });
    }

    public async Task<bool> IsValidList(string list)
    {
      return await Task.Run(() =>
      {
        list = list.ToUpper();

        // This misses RL intentionally as its used by ADD and REM

        if (list == "FL")
          return true;
        else if (list == "AL")
          return true;
        else if (list == "BL")
          return true;
        else
          return false;
      });


    }

    public async Task<bool> IsValidStatus(string status)
    {
      return await Task.Run(() =>
      {
        status = status.ToUpper();

        if (status == "NLN")
          return true;
        else if (status == "HDN")
          return true;
        else if (status == "BSY")
          return true;
        else if (status == "BRB")
          return true;
        else if (status == "AWY")
          return true;
        else if (status == "PHN")
          return true;
        else if (status == "LUN")
          return true;
        else
          return false;
      });
    }

    public async Task DoMSG(string[] split, string packet)
    {
      //TODO: Properly parse this info?

      // DO NOT USE IN PRODUCTION! EASILY EXPLOITABLE AS OF 31/05/20

      // First get the payload.
      await Task.Run(async () =>
      {
        try
        {
          string payload = await Task.Run(async () =>
          {
            char[] Buffer = new char[int.Parse(split[3])];
            await user.reader.ReadAsync(Buffer, 0, int.Parse(split[3]));

            return new string(Buffer);
          });

          // We should parse the payload to ensure its formatted correctly. Fuck that for now.
          foreach (string contact in user.MySession.Users)
          {
            if (contact == user.info.handle)
              continue;

            int check = await Program.Switchboard.GetOnlineUserIndex(contact);
            if (check != -1)
            {
              await Program.Switchboard.Users[check].Send($"MSG {user.info.handle} {user.info.nickname} " + Encoding.ASCII.GetBytes(payload).Length.ToString());
              await Program.Switchboard.Users[check].Send(payload + "\r\n\r\n\r\n");
            }
          }

          if (split[2] == "A")
            await user.Send($"ACK {split[1]}");

          //user.ShouldProcessPackets = true;
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.StackTrace);
        }
      });
    }

    public async Task DoCAL(string[] split, string packet)
    {
      //CAL 14 reece@hotmail.com
      await Task.Run(async () =>
      {
        if (await DB.UserExists(split[2]) == false)
        {
          await user.Send($"217 {split[1]}");
          return;
        }
        else
        {
          // Is user online or blocking me?
          int check = await Program.Notification.GetOnlineUserIndex(split[2]);

          if (check == -1)
          {
            await user.Send($"217 {split[1]}");
            return;
          }
          else
          {
            if (Program.Notification.Users[check].UserLists.BL.Contains(user.info.handle))
            {
              await user.Send($"217 {split[1]}");
              return;
            }
          }

          // Are we currently already in a session?
          if (user.MySession != null)
          {
            // Error!
            // Todo: Do something here!
            Program.Notification.Users[check].Protocol
    .SendRingTo(user.info.handle, split[2], user.MySession.SessionID,
    ConfigFileHandler.SwitchboardIP, (await user.MySession.NewKey(split[2])).key);

            await user.Send($"CAL {split[1]} RINGING {user.MySession.SessionID}");
          }
          else
          {
            // Check if the other user already opened a session.
            for (int i = 0; i < Program.Switchboard.Sessions.Count; ++i)
            {
              //TODO: We need to check if other user started to talk first and use that session.
              if (Program.Switchboard.Sessions[i].Starter == split[2] &&
              Program.Switchboard.Sessions[i].Users.Contains(user.info.handle) &&
              Program.Switchboard.Sessions[i].Users.Count == 2)
              {
                // Already in a session with this user.
                // We'll fake leaving it to rejoin it.
                int o = await Program.Switchboard.GetOnlineUserIndex(Program.Switchboard.Sessions[i].Starter);

                if (o != -1)
                {
                  user.MySession = Program.Switchboard.Sessions[i];

                  //await Program.Switchboard.Users[o].Send($"BYE {user.info.handle}");
                  await user.Send($"CAL {split[1]} RINGING {user.MySession.SessionID}");
                  await user.Send($"JOI {Program.Switchboard.Sessions[i].Starter} {await DB.GetUserNickname(Program.Switchboard.Sessions[i].Starter)}");
                  return;
                }
              }
            }

            user.MySession = await Program.Switchboard.GetSessionByID(
              await Program.Switchboard.CreateSession(user.info.handle));

            if (!user.MySession.Users.Contains(user.info.handle))
              user.MySession.Users.Add(user.info.handle);

            Program.Notification.Users[check].Protocol
    .SendRingTo(user.info.handle, split[2], user.MySession.SessionID,
    ConfigFileHandler.SwitchboardIP, (await user.MySession.NewKey(split[2])).key);

            await user.Send($"CAL {split[1]} RINGING {user.MySession.SessionID}");
          }
        }
      });
    }

    public async Task DoXFR(string[] split, string packet)
    {
      //TODO: Possibly support more than one switchboard server?
      //XFR 327 SB
      //XFR 327 SB 127.0.0.1:1863

      //This gets sent to target:
      //RNG [SessionID] [Address(:Port)] CKI [Key] [CallerAccount] [CallerNickname]
      await Task.Run(async () =>
      {
        if (await user.CanContinue(split[1]) == false)
          return;

        //string sessionid = await Program.Switchboard.CreateSession(user.info.handle);
        //Session session = await Program.Switchboard.GetSessionByID(sessionid);

        await user.Send($"XFR {split[1]} SB {ConfigFileHandler.SwitchboardIP} CKI {(await Program.Switchboard.CreateLoginKey(user.info.handle)).key}");
      });
    }

    public async Task DoREM(string[] split, string packet)
    {
      //REM 274 FL kael@hotmail.com
      await Task.Run(async () =>
      {
        if (await user.CanContinue(split[1]) == false)
          return;

        // First things first. Does this user exist?
        int check = await Program.Notification.GetOnlineUserIndex(split[3]);

        if (await DB.UserExists(split[3]) == false)
        {
          // Fail conditon.
          await user.Send($"201 {split[1]}");
          return;
        }

        if (await IsValidList(split[2]))
        {
          if (split[2] == "FL")
          {
            if (!user.UserLists.FL.Contains(split[3]))
            {
              await user.Send($"216 {split[1]}");
              return;
            }
            else
            {
              ++user.UserLists.Version;
              user.UserLists.FL.Remove(split[3]);

              if (check == -1)
              {
                UserLists list = await DB.GetUserLists(split[3]);
                if (list.RL.Contains(user.info.handle))
                  list.RL.Remove(user.info.handle);
                ++list.Version;
                await DB.SaveLists(list, null, split[3]);
              }
              else
              {
                Program.Notification.Users[check].UserLists.RL.Remove(user.info.handle);
                ++Program.Notification.Users[check].UserLists.Version;
                await Program.Notification.Users[check].Protocol.SendListChange(user.info.handle, split[3], "RL", false);
              }
              await user.Send($"REM {split[1]} FL {user.UserLists.Version} {split[3]}");
            }
          }
          else if (split[2] == "AL")
          {
            if (!user.UserLists.AL.Contains(split[3]))
            {
              await user.Send($"216 {split[1]}");
              return;
            }
            else
            {
              ++user.UserLists.Version;
              user.UserLists.AL.Remove(split[3]);

              await user.Send($"REM {split[1]} AL {user.UserLists.Version} {split[3]}");
            }
          }
          else if (split[2] == "BL")
          {
            if (!user.UserLists.BL.Contains(split[3]))
            {
              await user.Send($"216 {split[1]}");
              return;
            }
            else
            {
              ++user.UserLists.Version;
              user.UserLists.BL.Remove(split[3]);

              await user.Send($"REM {split[1]} BL {user.UserLists.Version} {split[3]}");
            }
          }
          else if (split[2] == "RL")
          {
            // Destroy connection.
            try
            {
              user.MySocket.Disconnect(false);
            }
            catch (Exception) { }
          }
          else
          {
            // Invalid list.
            await user.Send($"201 {split[1]}");
          }
        }
      });
    }

    public async Task DoADD(string[] split, string packet)
    {
      await Task.Run(async () =>
      {
        //ADD 165 FL kael@hotmail.com kael@hotmail.com
        if (await user.CanContinue(split[1]) == false)
          return;

        // First things first. Does this user exist?
        int check = await Program.Notification.GetOnlineUserIndex(split[3]);

        if (await DB.UserExists(split[3]) == false)
        {
          // Fail conditon.
          await user.Send($"201 {split[1]}");
          return;
        }

        // Is the list type valid?
        if (await IsValidList(split[2]))
        {
          // Hi dave!!
          if (split[2] == "FL")
          {
            if (user.UserLists.FL.Contains(split[3]))
            {
              // Fail. User already in list.
              await user.Send($"215 {split[1]}");
              return;
            }
            else
            {
              if (user.UserLists.BL.Contains(split[3]))
              {
                // User is on opposite list.
                await user.Send($"219 {split[1]}");
                return;
              }
              // Continue to add user to our list.
              user.UserLists.Version = user.UserLists.Version + 1;
              user.UserLists.FL.Add(split[3]);

              await user.Send($"ADD {split[1]} FL {user.UserLists.Version} {split[3]} {split[4]}");

              //Now add us to that users reverse list.
              if (check != -1)
              {
                if (Program.Notification.Users[check].IsLoggedIn)
                {
                  // Update name if not blocked.
                  if (!Program.Notification.Users[check].UserLists.BL.Contains(user.info.handle))
                    await user.Protocol.SendNameChange(Program.Notification.Users[check].info.handle, user.info.handle, Program.Notification.Users[check].info.nickname);

                  Program.Notification.Users[check].UserLists.Version += 1;
                  if (Program.Notification.Users[check].UserLists.RL.Contains(user.info.handle))
                    Program.Notification.Users[check].UserLists.RL.Remove(user.info.handle);
                  Program.Notification.Users[check].UserLists.RL.Add(user.info.handle);
                  await Program.Notification.Users[check].Protocol.SendListChange(user.info.handle, split[3], "RL", true);

                  if (!Program.Notification.Users[check].UserLists.BL.Contains(user.info.handle))
                  {
                    // Can't get this fucking shit to work like the real servers.
                    await user.Send($"NLN {Program.Notification.Users[check].Status} {Program.Notification.Users[check].info.handle} {Program.Notification.Users[check].info.nickname}");
                  }
                }
                else
                {
                  UserLists list = await DB.GetUserLists(split[3]);
                  if (list.RL.Contains(user.info.handle))
                    list.RL.Remove(user.info.handle);
                  list.RL.Add(user.info.handle);
                  await DB.SaveLists(list, null, split[3]);
                }
              }
              else
              {
                UserLists list = await DB.GetUserLists(split[3]);
                if (list.RL.Contains(user.info.handle))
                  list.RL.Remove(user.info.handle);
                list.RL.Add(user.info.handle);
                await DB.SaveLists(list, null, split[3]);
              }
            }
          }
          else if (split[2] == "AL")
          {
            if (user.UserLists.AL.Contains(split[3]))
            {
              // Fail. User already in list.
              await user.Send($"215 {split[1]}");
              return;
            }
            else
            {
              if (user.UserLists.BL.Contains(split[3]))
              {
                // User is on opposite list.
                await user.Send($"219 {split[1]}");
                return;
              }
              // Continue to add user to our list.
              user.UserLists.Version = user.UserLists.Version + 1;
              user.UserLists.AL.Add(split[3]);

              await user.Send($"ADD {split[1]} AL {user.UserLists.Version} {split[3]} {split[3]}");
            }
          }
          else if (split[2] == "BL")
          {
            if (user.UserLists.BL.Contains(split[3]))
            {
              // Fail. User already in list.
              await user.Send($"215 {split[1]}");
              return;
            }
            else
            {
              if (user.UserLists.AL.Contains(split[3]))
              {
                // User is on opposite list.
                await user.Send($"219 {split[1]}");
                return;
              }
              // Continue to add user to our list.
              user.UserLists.Version = user.UserLists.Version + 1;
              user.UserLists.BL.Add(split[3]);

              await user.Send($"ADD {split[1]} BL {user.UserLists.Version} {split[3]} {split[3]}");
            }
          }
          else if (split[3] == "RL")
          {
            // Destroy connection.
            try
            {
              user.MySocket.Disconnect(false);
            }
            catch (Exception) { }
          }
        }
        else
        {
          await user.Send($"201 {split[1]}");
        }
      });
    }

    public async Task DoANS(string[] split, string packet)
    {
      //ANS 3 reece@hotmail.com oOHOIG1V08YAps8oQCAZYxK0c7TqbCZTD8 1
      // Now verify the key with the session.
      await Task.Run(async () =>
      {
        try
        {
          for (int o = 0; o < Program.Switchboard.Sessions.Count; ++o)
          {
            if (Program.Switchboard.Sessions[o].SessionID == split[4])
            {
              foreach (Key key in Program.Switchboard.Sessions[o].Keys)
              {
                if (key.key == split[3] && key.user == split[2])
                {
                  // Success

                  user.MySession = Program.Switchboard.Sessions[o];

                  int check = await Program.Notification.GetOnlineUserIndex(split[2]);

                  if (check != -1)
                  {
                    user.info = Program.Notification.Users[check].info;
                    user.UserLists = Program.Notification.Users[check].UserLists;
                    user.IsLoggedIn = true;
                  }
                  else
                  {
                    // Probably kill the connection here as this makes no sense.
                  }

                  foreach (string user in user.MySession.Users)
                  {
                    // Alert users of new user.
                    int index = await Program.Switchboard.GetOnlineUserIndex(user);
                    await Program.Switchboard.Users[index].Protocol.SendUserJustJoined(this.user.info.handle, "", true, "");
                  }

                  if (!user.MySession.Users.Contains(user.info.handle))
                    user.MySession.Users.Add(user.info.handle);

                  // Now alert us of how many users exist in this chat.
                  for (int i = 0; i < user.MySession.Users.Count; ++i)
                  {
                    await Program.Switchboard.Users[i].Protocol.SendUserJustJoined(
                      user.MySession.Users[i], "", false, split[1], user.MySession.Users.Count, i+1);
                  }
                  
                  await user.Send($"ANS {split[1]} OK");
                  
                  break;
                }
                else
                  continue;
              }
            }
          }
        }
        catch (Exception ex) {
          Console.WriteLine(ex.StackTrace);
      }
      });
      user.ShouldProcessPackets = true;
    }

    public async Task DoQNG(string[] split, string packet)
    {
      // If the client fails this check we know to disconnect.
      await Task.Run(() => user.PassedPingCheck = true);
    }

    public async Task DoOUT(string[] split, string packet)
    {
      if (!Switchboard)
      {
        // Don't change user data if they didn't auth.
        await Task.Run(async () =>
        {
          if (!user.IsLoggedIn)
            user.info = null;
          else
          {
            foreach (string friend in user.UserLists.FL)
            {
              int check = await Program.Notification.GetOnlineUserIndex(friend);
              if (check != -1)
              {
                if (Program.Notification.Users[check].IsLoggedIn)
                  await Program.Notification.Users[check].Send($"FLN {user.info.handle}");
              }
            }
          }
        });
      }
      else
      {
        await Task.Run(async () =>
        {
          // We must inform other chat memebers if in a session.
          if (user.MySession != null)
          {
            foreach (string participant in user.MySession.Users)
            {
              if (participant == user.info.handle)
                continue;

              int check = await Program.Switchboard.GetOnlineUserIndex(participant);

              if (check != -1)
                await Program.Switchboard.Users[check].Send($"BYE {user.info.handle}");
            }
            user.MySession.Users.Remove(user.info.handle);
            if (user.MySession.Users.Count == 0)
            {
              // Empty session
              user.MySession.Keys.Clear();
              user.MySession.Users.Clear();
              Program.Switchboard.Sessions.Remove(user.MySession);
            }
            else if (user.MySession.Users.Count == 1)
            {
              // Silently kill this session.
              int us = await Program.Switchboard.GetOnlineUserIndex(user.MySession.Users[0]);

              if (us == -1)
              {
                user.MySession.Users.Clear();
                Program.Switchboard.Sessions.Remove(user.MySession);
              }
              else
              {
                await Program.Switchboard.Users[us].Send($"BYE {Program.Switchboard.Users[us].info.handle}");
                try
                {
                  Program.Switchboard.Users[us].MySocket.Disconnect(false);
                }
                catch (Exception ex) { }
                user.MySession.Users.Clear();
                Program.Switchboard.Sessions.Remove(user.MySession);
              }
            }
            user.MySession = null;
          }
        });
      }

      // Every client version seems to treat the OUT command differently.
      await Task.Run(() =>
      {
        try
        {
          user.MySocket.Disconnect(false);
        }
        catch (Exception)
        {
          // Client disconnected before we could boot them.
        }
      });
    }

    public async Task DoREA(string[] split, string packet)
    {
      await Task.Run(async () =>
      {
        if (await user.CanContinue(split[1]) == false)
          return;

        if (split[2] == user.info.handle)
        {
          user.info.nickname = split[3];
          ++user.UserLists.Version;

          await user.Send($"REA {split[1]} {user.UserLists.Version} {user.info.handle} {user.info.nickname}");

          // Now tell friends.
          foreach (string friend in user.UserLists.FL)
          {
            // We don't want to report out online presence to blocked friends
            if (user.UserLists.BL.Contains(friend))
              continue;

            int check1 = await Program.Notification.GetOnlineUserIndex(friend);
            if (check1 == -1)
            {
              // Bit of a hack. //
              UserLists Target = await DB.GetUserLists(friend);
              Target.Version = Target.Version + 1;
              await DB.SaveLists(Target, null, friend);
            }
            else
            {
              // They are online so we can inform them in real time!
              UserWrapper Target = Program.Notification.Users[check1];

              if (Target.IsLoggedIn)
              {
                Target.UserLists.Version = Target.UserLists.Version + 1;
                await Target.Protocol.SendNameChange(user.info.handle, Target.info.handle, user.info.nickname);//Send($"REA 0 {Target.UserLists.Version} {user.info.handle} {user.info.nickname}");
              }
              else
                continue;
            }
          }
        }
      });
    }

    public async Task DoFND(string[] split, string packet)
    {
      if (await user.CanContinue(split[1]) == false)
        return;

      // This command isn't very well documented. Disabled for now.
      await user.Send($"502 {split[1]}");
    }

    public async Task DoCHG(string[] split, string packet)
    {
      if (await user.CanContinue(split[1]) == false)
        return;

      if (await IsValidStatus(split[2]))
      {
        user.Status = split[2];
        await user.Send($"CHG {split[1]} {split[2]}");

        // Now alert friends if any.
        await Task.Run(async () =>
        {
          foreach (string friend in user.UserLists.FL)
          {
            // Don't report our state to blocked users.
            if (user.UserLists.BL.Contains(friend))
              continue;
            
            int check = await Program.Notification.GetOnlineUserIndex(friend);

            if (check != -1)
            {
              if (Program.Notification.Users[check].IsLoggedIn)
                await Program.Notification.Users[check].Protocol.SendStatusChange(user.info.handle, friend, user.Status);

            }
          }
        });
      }
      else
      {
        // Invalid status
        await user.Send($"201 {split[1]}");
      }
    }

    public async Task DoCVR(string[] split, string packet)
    {
      // TODO: Is this a okay method of responding to the CVR??
      await user.Send($"CVR {split[1]} 1.0.0000 2.2.1053");
    }

    public async Task DoSYN(string[] split, string packet)
    {
      // This is going to be a bitch to implement.

      string ClientContactVersion = split[2];

      // Really dirty hack. Force a re-sync
      if (int.Parse(ClientContactVersion) > user.UserLists.Version)
        user.UserLists.Version = int.Parse(ClientContactVersion) + 1;

      if (int.Parse(ClientContactVersion) < user.UserLists.Version)
      {
        await user.Send($"SYN {split[1]} {user.UserLists.Version}");

        // I guess we will have to fake users privacy settings for now. too bad!
        await user.Send($"GTC {split[1]} {user.UserLists.Version} A");
        await user.Send($"BLP {split[1]} {user.UserLists.Version} AL");

        await user.SendList("AL", split[1]);
        await user.SendList("FL", split[1]);
        await user.SendList("BL", split[1]);
        await user.SendList("RL", split[1]); // Is this one sync'd?
      }
      else
      {
        await user.Send($"SYN {split[1]} {split[2]}");
      }

      foreach (string friend in user.UserLists.FL)
      {
        int index = await Program.Notification.GetOnlineUserIndex(friend);

        if (index != -1)
        {
          if (Program.Notification.Users[index].IsLoggedIn)
          {
            // Alert our friend of our login
            await Program.Notification.Users[index].Send($"NLN NLN {user.info.handle} {user.info.nickname}");
          }
        }
      }

      // Now we need the status of each friend.
      foreach (string friend in user.UserLists.FL)
      {
        int index = await Program.Notification.GetOnlineUserIndex(friend);

        if (index != -1)
        {
          if (Program.Notification.Users[index].IsLoggedIn)
          {
            // TODO: get user status and send that instead here.
            await user.Send($"ILN {split[1]} NLN {friend} {Program.Notification.Users[index].info.nickname}");
          }
        }
        else
        {
          await user.Send($"ILN {split[1]} FLN {friend} {await DB.GetUserNickname(friend)}");
        }
      }
    }

    public async Task DoUSR(string[] split, string packet)
    {
      //C->S: USR [TransactionID] MD5 I [Account]

      if (!Switchboard)
      {
        await Task.Run(async () =>
        {
          // Already logged in.
          if (user.IsLoggedIn)
          {
            await user.Send($"207  {split[1]}");
            return;
          }
          if (split[2] != "MD5")
          {
            // Unsupported auth type.
            await user.Send($"201 {split[1]}");
          }
          else
          {
            // Supported.
            if (split[3] == "I")
            {
              // Client wants to start auth process.
              if (user.AuthChallenge != "" && user.AuthChallenge != null)
              {
                // We already have a challenge send it again.
                await user.Send($"USR {split[1]} MD5 S {user.AuthChallenge}");
              }
              else
              {
                if (await DB.UserExists(split[4]))
                {
                  // Generate challenge
                  user.AuthChallenge = await ChallengeMaker.MakeNormalChallenge();
                  user.info = await DB.GetUserInfo(split[4]);
                  await user.Send($"USR {split[1]} MD5 S {user.AuthChallenge}");
                }
                else
                {
                  await user.Send($"911 {split[1]}");
                }
              }
            }
            else if (split[3] == "S")
            {
              // Validate if they passed the challenge.
              if (await Hash.CreateMD5(user.AuthChallenge + user.info.password) == split[4])
              {
                // If we are logged in on another device we should logout. //

                if (!Switchboard)
                {
                  int check = await Program.Notification.GetOnlineUserIndex(user.info.handle);

                  if (check != -1 && Program.Notification.Users[check].IsLoggedIn)
                  {
                    await Program.Notification.Users[check].Send($"OUT OTH");
                    await Task.Run(() =>
                    {
                      try
                      {
                        Program.Notification.Users[check].MySocket.Disconnect(false);
                      }
                      catch (Exception)
                      {
                        // Ignore. Client disconnected for us.
                      }
                    });
                  }
                }

                user.IsLoggedIn = true;
                user.UserLists = await DB.GetUserLists(user.info.handle);
                await user.Send($"USR {split[1]} OK {user.info.handle} {user.info.nickname}");
              }
              else
              {
                // Ensure this is not wrong.
                user.IsLoggedIn = false;
                await user.Send($"911 {split[1]}");
              }
            }
            else
            {
              // Not expected.
              await user.Send($"201 {split[1]}");
            }
          }
        });
      }
      else
      {
        //USR 9 kael@hotmail.com 1025842060.584310268.531711792
        // Already logged in.
        if (user.IsLoggedIn)
        {
          await user.Send($"207  {split[1]}");
          return;
        }
        if (await DB.UserExists(split[2]) == false)
        {
          await user.Send($"911 {split[1]}");
          return;
        }
        else
        {
          foreach (Key key in Program.Switchboard.LoginKeys)
          {
            if (key.key == split[3])
            {
              // Success, now get user data.

              int check = await Program.Notification.GetOnlineUserIndex(split[2]);

              if (check != -1)
              {
                user.info = Program.Notification.Users[check].info;
              }
              else
              {
                user.info = await DB.GetUserInfo(split[2]);
              }
              user.IsLoggedIn = true;

              await user.Send($"USR {split[1]} OK {user.info.handle} {user.info.nickname}");
            }
          }
        }
      }
    }

    public async Task DoVER(string[] split, string packet)
    {
      // VER 4 MSNP4 CVR0
      // TODO: We need to actually implement this. //
      await user.Send($"VER {split[1]} MSNP3 CVR0");
    }

    public async Task DoINF(string[] split, string packet)
    {
      // We should consider the information sent to us.
      // So that we can properly process future packets. too bad!

      Utilty.SyntaxIssue check = Utilty.HasRequiredInputs(split, 2);

      if (check == Utilty.SyntaxIssue.Expected)
        await user.Send($"INF {split[1]} MD5"); // This is the same for every protocol version.
      else if (check == Utilty.SyntaxIssue.MissingInputs) // Required field missing
        await user.Send($"300 {split[1]}");
      else if (check == Utilty.SyntaxIssue.TooManyInputs)
        await user.Send($"201 {split[1]}"); // Invalid Parameter, is this the right code? Probably not.

    }

    public async Task UnknownPacket(string[] split, string packet)
    {
      user.BadPackets = user.BadPackets + 1;
      // We should probably count these per minute so we can disconnect bad clients.
      await AsyncSupport.SetForegroundColour(ConsoleColor.Red);
      await AsyncSupport.WriteLine($"--- Encountered unknown or unimplemented packet ---");
      await AsyncSupport.WriteLine(packet);
      await AsyncSupport.WriteLine($"---------------------------------------------------");
      await AsyncSupport.SetForegroundColour(ConsoleColor.White);
    }

    public async Task ProtocolNumber()
    {
      await Task.Run(() => { return 1; });
    }

    public async Task ProtocolString()
    {
      await Task.Run(() => { return "MSNP1"; });
    }

    public async Task SendNameChange(string from, string to, string name)
    {
      await user.Send($"REA 0 {user.UserLists.Version} {from} {name}");
    }

    public async Task SendStatusChange(string from, string to, string status)
    {
      int check = await Program.Notification.GetOnlineUserIndex(from);
      if (status == "HDN")
        await user.Send($"NLN FLN {from} {Program.Notification.Users[check].info.nickname}");
      else
        await user.Send($"NLN {status} {from} {Program.Notification.Users[check].info.nickname}");
    }

    public Task LogonStatusChange(string from, string to, string status)
    {
      throw new NotImplementedException();
    }

    public Task LogoffStatusChange(string from, string to, string status)
    {
      throw new NotImplementedException();
    }

    public async Task SendListChange(string from, string to, string list, bool AddToList)
    {
      if (AddToList)
        await user.Send($"ADD 0 {list} {user.UserLists.Version} {from} {from}");
      else
        await user.Send($"REM 0 {list} {user.UserLists.Version} {from} {from}");
    }

    public async Task SendRingTo(string from, string to, string sessionid, string switchboardip, string key)
    {
      ////RNG [SessionID] [Address(:Port)] CKI [Key] [CallerAccount] [CallerNickname]
      await user.Send($"RNG {sessionid} {switchboardip} CKI {key} {from} {await DB.GetUserNickname(from)}");
    }

    public Task SendMsgPayload(string from, string to, PayloadMSG msg)
    {
      throw new NotImplementedException();
    }

    public async Task IsSwitchboard(bool sb)
    {
      await Task.Run(() => Switchboard = sb);
    }

    public async Task SendUserJustJoined(string from, string to, bool AlreadyExistingUser,string transaction, int max = -1, int current = -1)
    {
      if (AlreadyExistingUser)
        await user.Send($"JOI {from} {await DB.GetUserNickname(from)}");
      else
        await user.Send($"IRO {transaction} {current} {max} {from} {await DB.GetUserNickname(from)}");
    }
  }
}
