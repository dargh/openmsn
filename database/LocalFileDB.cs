﻿using OpenMessengerNET.database.wrappers;
using OpenMessengerNET.utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

/*
 * 
 * DO NOT USE THIS DATABASE IN PRODUCTION!!
 * THIS IS A DEVELOPMENT TEST DATABASE NOT INTENDED TO BE SECURE.
 * 
 */
namespace OpenMessengerNET.database
{
  class LocalFileDB : IDatabaseType
  {
    public async Task<string> CleanString(string str)
    {
      return await Task.Run(() =>
      {
        // There is a better way of doing this. God help me.
        string output = str.Replace("/", "");
        output = output.Replace("\r\n", "");
        output = output.Replace(" ", "");
        output = output.Replace("%", "");
        output = output.Replace("$", "");
        output = output.Trim();
        return output;
      });
    }

    public async Task CreateTestAccounts()
    {
      await Task.Run(async () =>
      {
        // Right if this method is called we should check if the db folder exists.
        if (!Directory.Exists(ConfigFileHandler.LocalFileDatabasePath))
        {
          // Shit.. Quickly throw one together and hope dad doesn't notice.
          Directory.CreateDirectory(ConfigFileHandler.LocalFileDatabasePath);
          Directory.CreateDirectory(ConfigFileHandler.LocalFileDatabasePath+"/users/");
          Directory.CreateDirectory(ConfigFileHandler.LocalFileDatabasePath+"/lists/");

          UserInfo dummy = new UserInfo();
          dummy.handle = "reece@hotmail.com";
          dummy.nickname = "Reece";
          dummy.password = "1234";
          dummy.isBanned = false;

          UserLists dummy2 = new UserLists();

          dummy2.AL.Add("joe@hotmail.com");
          dummy2.FL.Add("joe@hotmail.com");
          dummy2.RL.Add("joe@hotmail.com");


          await SaveLists(dummy2, dummy);
          await SaveUser(dummy);

          dummy2 = new UserLists();

          dummy2.AL.Add("reece@hotmail.com");
          dummy2.FL.Add("reece@hotmail.com");
          dummy2.RL.Add("reece@hotmail.com");

          dummy.handle = "joe@hotmail.com";
          dummy.nickname = "Joe";
          dummy.password = "1234";
          dummy.isBanned = false;
          await SaveUser(dummy);
          await SaveLists(dummy2, dummy);

          dummy2 = new UserLists();
          dummy.handle = "kael@hotmail.com";
          dummy.nickname = "Kael";
          dummy.password = "1234";
          dummy.isBanned = false;
          await SaveUser(dummy);
          await SaveLists(dummy2, dummy);

          dummy2 = new UserLists();
          dummy.handle = "rud@hotmail.com";
          dummy.nickname = "Rud";
          dummy.password = "1234";
          dummy.isBanned = false;
          await SaveUser(dummy);
          await SaveLists(dummy2, dummy);

          dummy2 = new UserLists();
          dummy.handle = "ayuugo@hotmail.com";
          dummy.nickname = "AyuuGo";
          dummy.password = "1234";
          dummy.isBanned = false;
          await SaveUser(dummy);
          await SaveLists(dummy2, dummy);

          dummy2 = new UserLists();
          dummy.handle = "cyanox@hotmail.com";
          dummy.nickname = "cyanox";
          dummy.password = "1234";
          dummy.isBanned = false;
          await SaveUser(dummy);
          await SaveLists(dummy2, dummy);
        }
      });
    }

    public async Task<string> DatabaseName()
    {
      return await Task.Run(() => { return "FileStorage"; });
    }

    public async Task<UserInfo> GetUserInfo(string email)
    {
      return await Task.Run(async () =>
      {
        // Cleanup string first.
        try
        {
          string clean = await CleanString(email);

          using BinaryReader reader = new BinaryReader(File.OpenRead(ConfigFileHandler.LocalFileDatabasePath + "/users/" + await Hash.CreateMD5(clean)));
          UserInfo temp = new UserInfo();

          temp.isBanned = reader.ReadBoolean();
          temp.handle = reader.ReadString();
          temp.nickname = reader.ReadString();
          temp.password = reader.ReadString();

          return temp;
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex);
          return null;
        }
      });
    }

    public async Task<UserLists> GetUserLists(string email)
    {
      string Index = await Hash.CreateMD5(email);
      string List1 = await Hash.CreateMD5(email + "AL");
      string List2 = await Hash.CreateMD5(email + "BL");
      string List3 = await Hash.CreateMD5(email + "FL");
      string List4 = await Hash.CreateMD5(email + "RL");

      return await Task.Run(() =>
      {
        UserLists info = new UserLists();

        using BinaryReader breader = new BinaryReader(File.OpenRead(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + Index));

        info.Version = breader.ReadInt32();
        breader.Dispose();

        foreach (string handle in File.ReadAllLines(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List1))
        {
          info.AL.Add(handle);
        }

        foreach (string handle in File.ReadAllLines(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List2))
        {
          info.BL.Add(handle);
        }

        foreach (string handle in File.ReadAllLines(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List3))
        {
          info.FL.Add(handle);
        }

        foreach (string handle in File.ReadAllLines(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List4))
        {
          info.RL.Add(handle);
        }

        return info;
      });
    }

    public async Task<string> GetUserNickname(string handle)
    {
      // THIS IS REALLY FUCKING DUMB, WOW!
      UserInfo user = await GetUserInfo(handle);
      return await Task.Run(() => user.nickname);
    }

    public async Task<bool> IsConnected()
    {
      return await Task.Run(() =>
      {
        return File.Exists(ConfigFileHandler.LocalFileDatabasePath);
      });
    }

    public async Task<bool> SaveLists(UserLists lists, UserInfo info, string email = null)
    {
      return await Task.Run(async () =>
      {
        string Index;
        string List1;
        string List2;
        string List3;
        string List4;

        if (email != null)
        {
          Index = await Hash.CreateMD5(email);
          List1 = await Hash.CreateMD5(email + "AL");
          List2 = await Hash.CreateMD5(email + "BL");
          List3 = await Hash.CreateMD5(email + "FL");
          List4 = await Hash.CreateMD5(email + "RL");
        }
        else
        {
          Index = await Hash.CreateMD5(info.handle);
          List1 = await Hash.CreateMD5(info.handle + "AL");
          List2 = await Hash.CreateMD5(info.handle + "BL");
          List3 = await Hash.CreateMD5(info.handle + "FL");
          List4 = await Hash.CreateMD5(info.handle + "RL");
        }

        if (File.Exists(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + Index))
          File.Delete(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + Index);

        if (File.Exists(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List1))
          File.Delete(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List1);

        if (File.Exists(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List2))
          File.Delete(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List2);

        if (File.Exists(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List3))
          File.Delete(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List3);

        if (File.Exists(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List4))
          File.Delete(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List4);

        BinaryWriter writeri = new BinaryWriter(File.OpenWrite(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + Index));

        writeri.Write(lists.Version);
        writeri.Flush();

        writeri.Dispose();
        StreamWriter writer = new StreamWriter(File.OpenWrite(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List1));

        foreach (string handle in lists.AL.ToArray())
        {
          writer.WriteLine(handle);
        }
        writer.Flush();

        writer.Dispose();
        writer = new StreamWriter(File.OpenWrite(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List2));

        foreach (string handle in lists.BL.ToArray())
        {
          writer.WriteLine(handle);
        }
        writer.Flush();

        writer.Dispose();
        writer = new StreamWriter(File.OpenWrite(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List3));

        foreach (string handle in lists.FL.ToArray())
        {
          writer.WriteLine(handle);
        }
        writer.Flush();

        writer.Dispose();
        writer = new StreamWriter(File.OpenWrite(ConfigFileHandler.LocalFileDatabasePath + "/lists/" + List4));

        foreach (string handle in lists.RL.ToArray())
        {
          writer.WriteLine(handle);
        }
        writer.Flush();
        writer.Dispose();

        return false;
      });
    }

    public async Task<bool> SaveUser(UserInfo info)
    {
      string savename = await Hash.CreateMD5(info.handle);

      return await Task.Run(() =>
      {
        using BinaryWriter writer = new BinaryWriter(File.OpenWrite(ConfigFileHandler.LocalFileDatabasePath + "/users/"+savename));

        try
        {
          writer.Write(info.isBanned);
          writer.Write(info.handle);
          writer.Write(info.nickname);
          writer.Write(info.password);
          writer.Flush();
        }
        catch (Exception)
        {
          //Oh fuck this is bad because we just deleted them.
          return false;
        }
        return true;
      });
    }

    public async Task<bool> UserExists(string email)
    {
      return await Task.Run(async () =>
      {
        if (File.Exists(ConfigFileHandler.LocalFileDatabasePath + "/users/" + await Hash.CreateMD5(email)))
          return true;
        else
          return false;
      });
    }
  }
}
