﻿using OpenMessengerNET.database.wrappers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OpenMessengerNET.database
{
  // If we use an interface we can implement multiple
  // database methods with ease as all must respect these functions.
  public interface IDatabaseType
  {
    Task CreateTestAccounts();
    Task<string> DatabaseName();
    Task<bool> UserExists(string email);
    Task<UserInfo> GetUserInfo(string email);
    Task<UserLists> GetUserLists(string email);
    Task<bool> IsConnected();

    Task<string> GetUserNickname(string handle);

    Task<bool> SaveUser(UserInfo info);
    Task<bool> SaveLists(UserLists lists, UserInfo info, string email = null);
  }
}
