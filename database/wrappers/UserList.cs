﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMessengerNET.database.wrappers
{
  /// <summary>
  /// Used to represent the different msn lists such as:
  /// AL
  /// FL
  /// BL
  /// </summary>
  public class UserLists
  {
    public int Version = 1;
    /// <summary>
    /// Allow List
    /// </summary>
    public List<string> AL = new List<string>();
    /// <summary>
    /// Friends List
    /// </summary>
    public List<string> FL = new List<string>();
    /// <summary>
    /// Block List
    /// </summary>
    public List<string> BL = new List<string>();

    // ffs
    public List<string> RL = new List<string>();
  }
}
