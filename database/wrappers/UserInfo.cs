﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMessengerNET.database.wrappers
{
  public class UserInfo
  {
    /// <summary>
    /// Is the user current banned?
    /// </summary>
    public bool isBanned = false;
    /// <summary>
    /// User Email.
    /// </summary>
    public string handle = null;
    /// <summary>
    /// User's display name.
    /// </summary>
    public string nickname = null;
    /// <summary>
    /// User password. Should be null'd after we've done the auth process.
    /// </summary>
    public string password = "";
  }
}
