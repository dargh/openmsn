﻿using OpenMessengerNET.database.wrappers;
using OpenMessengerNET.network.core;
using OpenMessengerNET.network.protocols;
using OpenMessengerNET.utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace OpenMessengerNET
{
  public class UserWrapper
  {
    // Why is this needed?
    // Well users send payloads which are expected packets.
    // We should stop listening for new packets when reading those. otherwise we might mix them up for real packets..
    public bool ShouldProcessPackets = true;
    public bool PassedPingCheck = true;
    public int BadPackets = 0;

    public Socket MySocket;
    public TcpClient Client;

    public StreamReader reader;
    public StreamWriter writer;

    public IProtocol Protocol;
    public string Status = "FLN";

    // Not sure where to put these but it should be fine here for now.
    public bool IsLoggedIn = false;
    public string AuthChallenge = null;
    public Session MySession = null;
    public UserInfo info;
    public UserLists UserLists = new UserLists();

    public UserWrapper(Socket sock)
    {
      MySocket = sock;
      // This is utterly fucking retarded, too bad!
      TcpClient temp = new TcpClient();
      temp.Client = sock;
      Client = temp;
      //
      reader = new StreamReader(Client.GetStream());
      writer = new StreamWriter(Client.GetStream());

      //Tadaa!
      Protocol = new ProtocolBase(this);
    }

    /// <summary>
    /// This purely is intended for commands that require you to be logged in.
    /// </summary>
    /// <returns></returns>
    public async Task<bool> CanContinue(string transaction)
    {
      return await Task.Run(async () =>
      {
        if (!IsLoggedIn)
        {
          BadPackets = BadPackets + 1;
          await Send($"302 {transaction}");
          return false;
        }
        else
        {
          return true;
        }
      });
    }

    public async Task SendList(string list, string transaction)
    {
      await Task.Run(async () =>
      {
        list = list.ToUpper();
        List<string> mylist;


        if (list == "AL")
          mylist = UserLists.AL;
        else if (list == "FL")
          mylist = UserLists.FL;
        else if (list == "BL")
          mylist = UserLists.BL;
        else if (list == "RL")
          mylist = UserLists.RL;
        else
          return;

        if (mylist.Count == 0)
        {
          await Send($"LST {transaction} {list} {UserLists.Version} 0 0");
        }
        else
        {
          int i = 1;
          foreach (string handle in mylist)
          {
            //TODO: We need a really effcient way to load nicknames without having to load all the user data...
            await Send($"LST {transaction} {list} {UserLists.Version} {i} {mylist.Count} {handle} {await Program.Notification.DB.GetUserNickname(handle)}");
            i++;
          }
        }
      });
    }

    public async Task Send(string text)
    {
      await writer.WriteLineAsync(text);
      await writer.FlushAsync();
      await AsyncSupport.WriteLine($"S: {text}");
    }

    public async Task<bool> IsConnected()
    {
     return await Task.Run(() =>
      {
        try
        {
          return !(MySocket.Poll(1, SelectMode.SelectRead) && MySocket.Available == 0);
        }
        catch (SocketException) { return false; }
        catch (ObjectDisposedException) { return false; }
      });
    }
  }
}
